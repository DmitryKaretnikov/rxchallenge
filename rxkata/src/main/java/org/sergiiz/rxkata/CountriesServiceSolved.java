package org.sergiiz.rxkata;

import java.util.List;
import java.util.Map;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Single;

class CountriesServiceSolved implements CountriesService {
    public Single<String> countryNameInCapitals(Country country) {
        return Single.just(country)
                .map(Country::getName)
                .map(String::toUpperCase);
    }

    public Single<Integer> countCountries(List<Country> countries) {
        return Single.just(countries)
                .flatMap(items -> Observable.fromIterable(items)
                        .count()
                        .map(Long::intValue));
    }

    public Observable<Long> listPopulationOfEachCountry(List<Country> countries) {
        return Observable.fromIterable(countries)
                .map(Country::getPopulation);
    }

    public Observable<String> listNameOfEachCountry(List<Country> countries) {
        return Observable.fromIterable(countries)
                .map(Country::getName);
    }

    public Observable<Country> listOnly3rdAnd4thCountry(List<Country> countries) {
        return Observable.fromIterable(countries)
                .skip(2)
                .take(2);
    }

    public Single<Boolean> isAllCountriesPopulationMoreThanOneMillion(List<Country> countries) {
        return Observable.fromIterable(countries)
                .map(Country::getPopulation)
                .all(item -> item > 1000000);
    }

    public Observable<Country> listPopulationMoreThanOneMillion(List<Country> countries) {
        return Observable.fromIterable(countries)
                .filter(item -> item.getPopulation() > 1000000);
    }

    public Observable<Country> listPopulationMoreThanOneMillionWithTimeoutFallbackToEmpty(final FutureTask<List<Country>> countriesFromNetwork) {
        return Observable.fromFuture(countriesFromNetwork)
                .timeout(100, TimeUnit.MILLISECONDS)
                .flatMap(this::listPopulationMoreThanOneMillion)
                .onErrorResumeNext(Observable.empty())
                .doOnComplete(() -> countriesFromNetwork.cancel(true));
    }

    public Observable<String> getCurrencyUsdIfNotFound(String countryName, List<Country> countries) {
        return Observable.fromIterable(countries)
                .filter(item -> item.getName().equals(countryName))
                .map(Country::getCurrency)
                .defaultIfEmpty("USD");
    }

    public Observable<Long> sumPopulationOfCountries(List<Country> countries) {
        return Observable.fromIterable(countries)
                .map(Country::getPopulation)
                .reduce((itemX, itemY) -> itemX + itemY)
                .toObservable();
    }

    public Single<Map<String, Long>> mapCountriesToNamePopulation(List<Country> countries) {
        return Single.just(countries)
                .flatMap(items -> Observable.fromIterable(items)
                        .toMap(Country::getName, Country::getPopulation));
    }

    public Observable<Long> sumPopulationOfCountries(Observable<Country> countryObservable1,
                                                     Observable<Country> countryObservable2) {
        return countryObservable1
                .concatWith(countryObservable2)
                .map(Country::getPopulation)
                .reduce((itemX, itemY) -> itemX + itemY)
                .toObservable();
    }

    public Single<Boolean> areEmittingSameSequences(Observable<Country> countryObservable1,
                                                    Observable<Country> countryObservable2) {
        return Observable.sequenceEqual(countryObservable1, countryObservable2);
    }
}
